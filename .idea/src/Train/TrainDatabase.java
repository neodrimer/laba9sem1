package Train;

import java.time.LocalTime;

public class TrainDatabase {

    private Train[] trains;
    public TrainDatabase(Train[] trains) {
        this.trains = trains;
    }
    private Train[] extractSubArray(Train[] source, int count){
        Train[] destination = new Train[count];
        System.arraycopy(source,0,destination,0,count);
        return destination;
    }
    public Train[] sortByUniqNumber(){
        Train[] result = trains;
        int len = result.length;
        for (int i=0;i<len-1;++i){
            for(int j=0;j<len-i-1; ++j){
                if(result[j+1].getUniq_number()<result[j].getUniq_number()){
                    Train swap = result[j];
                    result[j] = result[j+1];
                    result[j+1] = swap;
                }
            }
        }

        return result;
    }


    public Train[] sortByDepartureTime() {
        Train[] result = trains;
        int len = result.length;
        String time_departure1;
        String time_departure2;

        for (int i = 0; i < len - 1; ++i) {
            for (int j = 0; j < len - i - 1; ++j) {
                time_departure1 = result[j + 1].getHour_departure() + ":" + result[j + 1].getMinutes_departure();
                time_departure2 = result[j].getHour_departure() + ":" + result[j].getMinutes_departure();
                LocalTime time1 = LocalTime.parse(time_departure1);
                LocalTime time2 = LocalTime.parse(time_departure2);
                if (time1.isBefore(time2)) {
                    Train swap = result[j];
                    result[j] = result[j + 1];
                    result[j + 1] = swap;
                }
            }
        }

        return result;
    }
    public Train[] sortByArrival(){
        Train[] result = trains;
        int len = result.length;
        for (int i=0;i<len-1;++i){

            for(int j=0;j<len-i-1; ++j){

                if(result[j+1].getUniq_number()<result[j].getUniq_number()){

                    Train swap = result[j];
                    result[j] = result[j+1];
                    result[j+1] = swap;
                }
            }
        }

        return result;
    }
    public Train[] filterByDeparture(String departure){
        Train[] result = new Train[trains.length];
        int count=0;
        for(Train train: trains){
            if (train!=null && train.getDeparture()==departure){
                result[count++]=train;
            }
        }
        Train[] destination = extractSubArray(result,count);
        return destination;
    }

    public Train[] filterByArrival(String arrival){
        Train[] result = new Train[trains.length];
        int count=0;

        for(Train train: trains){
            if (train!=null && train.getArrival()==arrival){
                result[count++]=train;
            }
        }

        Train[] array = extractSubArray(result,count);

        int len = array.length;
        for (int i=0;i<len-1;++i){
            for(int j=0;j<len-i-1; ++j){
                if( array[j+1].getUniq_number()<array[j].getUniq_number()){
                    Train swap = array[j];
                    array[j] = array[j+1];
                    array[j+1] = swap;
                }
            }
        }

        Train[] destination = extractSubArray(array,count);
        return destination;
    }

    public Train filterByUniqNumber(int uniq_number){
        int count=0;
        for(Train train: trains){
            if (train!=null && train.getUniq_number()==uniq_number){
                return train;
            }
        }
        return null;
    }
}
