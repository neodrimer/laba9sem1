package Train;

import java.util.Objects;

public class Train {
    private int id;
    private String name;
    private int uniq_number;

    private String departure;
    private String arrival;


    private int hour_departure;
    private int minutes_departure;



    public Train(int id, String name, int uniq_number, String departure, String arrival, int hour_departure, int minutes_departure) {
        this.id = id;
        this.name = name;
        this.uniq_number = uniq_number;
        this.departure = departure;
        this.arrival = arrival;
        this.hour_departure = hour_departure;
        this.minutes_departure = minutes_departure;
    }


    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUniq_number() {
        return uniq_number;
    }

    public void setUniq_number(int uniq_number) {
        this.uniq_number = uniq_number;
    }

    public int getHour_departure() {
        return hour_departure;
    }

    @Override
    public String toString() {
        return "Train.Train{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", uniq_number=" + uniq_number +
                ", departure='" + departure + '\'' +
                ", arrival='" + arrival + '\'' +
                ", hour_departure=" + hour_departure +
                ", minutes_departure=" + minutes_departure +
                '}';
    }

    public void setHour_departure(int hour_departure) {
        this.hour_departure = hour_departure;
    }

    public int getMinutes_departure() {
        return minutes_departure;
    }

    public void setMinutes_departure(int minutes_departure) {
        this.minutes_departure = minutes_departure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Train train = (Train) o;

        if (id != train.id) return false;
        if (uniq_number != train.uniq_number) return false;
        if (hour_departure != train.hour_departure) return false;
        if (minutes_departure != train.minutes_departure) return false;
        return Objects.equals(name, train.name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + uniq_number;
        result = 31 * result + hour_departure;
        result = 31 * result + minutes_departure;
        return result;
    }

}

