package Customer;

import java.util.Arrays;
import java.util.Objects;

public class Customer {
    private int id;
    private String surname;
    private String name;

    private String partronycname;
    private String address;
    private long cardnumber;

    private String banknumber;

    private String fullname;

    public Customer(int id, String surname, String name, String partronycname, String address, long cardnumber, String banknumber) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.partronycname = partronycname;
        this.address = address;
        this.cardnumber = cardnumber;
        this.banknumber = banknumber;
        this.fullname = surname + name + partronycname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartronycname() {
        return partronycname;
    }

    public void setPartronycname(String partronycname) {
        this.partronycname = partronycname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(long cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getBanknumber() {
        return banknumber;
    }

    public void setBanknumber(String banknumber) {
        this.banknumber = banknumber;
    }

    @Override
    public String toString() {
        return "Customer.Customer{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", partronycname='" + partronycname + '\'' +
                ", address='" + address + '\'' +
                ", cardnumber=" + cardnumber +
                ", banknumber='" + banknumber + '\'' +
                ", fullname='" + fullname + '\'' +
                '}';
    }
}

