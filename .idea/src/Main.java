import Student.*;
import Book.*;
import Customer.*;
import Train.*;
public class Main {


    public static void main(String[] args) {

        //Task 1

        System.out.println("\nStudents:\n");
        Student Viktor = new Student("Viktor", "Seleznev", 1,new int[]{1,2,3,4,5,6});
        Student Andrey = new Student("Andrey", "Pupkov", 2,new int[]{9,9,10,9,9});
        Student Misha = new Student("Misha", "Antonov", 1,new int[]{10,10,10,10,10});
        StudentPrinter studentPrinter = new StudentPrinter();
        StudentRepository StudentRepository = new StudentRepository(new Student[]{Viktor,Andrey,Misha});

        System.out.println("Student.Student with grades more than 9:");
        Student[] StudentsWithGradesMoreThan9 = StudentRepository.findStudentsByGradesMoreThan(9);
        studentPrinter.print(StudentsWithGradesMoreThan9);


        // Task2

        System.out.println("\nTrains:");
        Train MoscowPetersburg = new Train(1,"Moscow-Petersburg-1", 10002,"Moscow","Petersburg",15,30);
        Train MinskVilnius = new Train(2,"Minsk-Vilnius-1",10001,"Minsk", "Vilnius",19,25);
        Train MoscowAstana = new Train(3,"Moscow-Astana-1",10003,"Moscow", "Astana",17,30);
        Train MinskAstana = new Train(4,"Minsk-Astana-1",10004,"Minsk", "Astana",17,35);

        TrainDatabase trainDatabase = new TrainDatabase(new Train[]{MoscowPetersburg,MinskVilnius,MoscowAstana,MinskAstana});
        System.out.println("\nSorted array of trains by uniq_number:");
        Train[] sortedArrayOfTrains = trainDatabase.sortByUniqNumber();
        TrainPrinter trainPrinter = new TrainPrinter();
        trainPrinter.print(sortedArrayOfTrains);

        System.out.println("\nDepartures from Moscow:");
        Train[] filteredDepartureFromMoscow = trainDatabase.filterByDeparture("Moscow");
        trainPrinter.print(filteredDepartureFromMoscow);

        System.out.println("\nTrain.Train with uniq_number 10003:");
        Train filterByUniqNumber = trainDatabase.filterByUniqNumber(10003);
        trainPrinter.print(filterByUniqNumber);

        System.out.println("\nTrains to Astana:");
        Train[] filteredByArrivalToAstana = trainDatabase.filterByArrival("Astana");
        trainPrinter.print(filteredByArrivalToAstana);

        System.out.println("\nTrains sorted to by Departure time:");
        Train[] sortedByDepartureTime = trainDatabase.sortByDepartureTime();
        trainPrinter.print(sortedByDepartureTime);

        //Task 3
        System.out.println("\nCustomers:");
        Customer siblamer = new Customer(0,"Seleznev","Viktor","Andreevich","Vilnius",22813371488l,"704");
        Customer jesus = new Customer(1,"Jesus","Is","God","Bethlehem ",11111111111l,"666");
        Customer morningstar = new Customer(2,"Morningstar","Devil","","Hell ",66666666666l,"777");

        System.out.println("\nSorted customers by fullname:");
        CustomerDatabase customerDatabase = new CustomerDatabase(new Customer[]{siblamer,jesus,morningstar});
        Customer[] sortedBuFullname = customerDatabase.sortedByFullname();
        CustomerPrinter customerPrinter = new CustomerPrinter();
        customerPrinter.print(sortedBuFullname);

        System.out.println("\nCustomers with cardnumber between 66666666665 and 66666666667:");
        Customer[] withCardnumberBetween = customerDatabase.filterByCardDiaposon(66666666665l,66666666667l);
        customerPrinter.print(withCardnumberBetween);

        // Task4


        System.out.println("\nBooks:");
        Book Java = new Book(1,"Java", new String[]{"Gosling"},"Piter",2010,800,60,"hard");
        Book sharp = new Book(2,"Java", new String[]{"Shildt"},"Piter",2022,40,75,"hard");

        BookRepository bookRepository = new BookRepository(new Book[]{Java,sharp});
        BookPrinter printer = new BookPrinter();
        Book[] goslingBooks = bookRepository.findByAuthor("Gosling");
        System.out.println("\nGosling books:");
        printer.print(goslingBooks);
        System.out.println("\nBooks from 2022:");
        Book[] booksByYear = bookRepository.findByYear(2022);
        printer.print(booksByYear);
        System.out.println("\nPiter's books:");
        Book[] piterBooks = bookRepository.findByPublisher("Piter");
        printer.print(piterBooks);



    }
}

